# Teamwork comparison

| Topic  | Company name 1 | Company name 2 |
| -- | -- | -- | -- |
| Issue tracking | Notes on the wall | Internal tool |
| Daily standup | ✓ | 𐄂 |
| Sprint duration | 2 weeks |No sprints |
| Developers in the team | 4 | 8 |
| Issue size estimation | Planning poker | By the scrum master |
| Code reviews | ....
| Revision control system | ...
| Revision control workflow | ...

*TODO:* The items above are just to get you started. You will need to add more topics and you may want to change the topics that are already there. There should be at least 15. Try to keep texts short (less than 10 words). Your goal is for the viewer to understand the most important differences between the development processes of these companies at a glance.

In case you have experienced more than two companies as a software developer, please feel encouraged to add columns!
