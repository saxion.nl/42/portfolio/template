# *TODO* (Company name)


## Roles

### *TODO* (Role name 1)

*TODO* (description and purpose, background, collaboration)

### *TODO* (Role name 2)

*TODO* (description and purpose, background, collaboration)

### *TODO* Etc...


## Development process

*TODO* (description ~800 words)


## Recommendations

*TODO* (your recommendations for improving the development process)


## Response by a senior developer at the company

*TODO:* (This section may be left out for one of the three companies.)

- Name: TODO
- Job title: TODO
- Date: TODO

### Regarding roles and description

*TODO* (copy/paste verbatim from what he/she wrote)

### Regarding your recommendations

*TODO* (copy/paste verbatim from what he/she wrote)


-------------

**NOTE:** Feel free to change the above outline to better suit your work.

