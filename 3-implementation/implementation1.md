## *TODO* (Project name)

### Project description

*TODO*

### Functionality description

*TODO* (You may want to link to screenshots/recording in the current directory.)

### Technical design

*TODO* (Link/copy any technical designs that you used as input to your implementation. If there were none, please state so.)

### Source code

*TODO* (Link to a `zip` of the source code (preferably) or a `patch` in the current directory. Make sure any documentation, testing and version control history (the `.git` directory) are included.)

### Source code before feedback

*TODO* (This first version of your source code that you created by yourself, before any reviews happened. (This can be a branch named `first-version` in your project's repository.))

### Help received

*TODO* (Describe any help you received while creating what's described in this portfolio part, including help from coworkers, classmates, friends, internet communities (passively reading forums doesn't need to be mentioned of course) or anybody else. Regular feedback from your graduation supervisor may be ignored, but if he/she contributed a very significant idea you should mention that as well. If you did it all by yourself, please state so explicitly. Failing to mention significant help received will be considered exam fraud.)


-------------

**NOTE:** Feel free to change the above outline to better suit your work.

