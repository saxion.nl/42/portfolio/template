# Portfolio progress

*Please make sure the tables below are up-to-date in your GitLab project before each biweekly meeting with your graduation supervisor.*

| Student | [name] |
| ------- | -- |
| Graduation supervisor | [name] |
| Expected graduation date | [date] |


## Planning

| Activity            | Company/subject | Start      | End        |
| ------------------- | --------------- | ---------- | ---------- |
| Internship 1        |                 |            |            |
| Free project 1      |                 |            |            |
| Internship 2        |                 |            |            |
| Free project 2      |                 |            |            |

Add any modules you may still need to finish to the above table.


## Progress

| Qualification part  | Status | Subject |
| ------------------- | ------ | - |
| Journal             | 0/100  | - |
| Functional design   | -      | - |
| Technical design    | -      | - |
| Implementation 1    | -      | - |
| Implementation 2    | -      | - |
| Problem solving     | -      | - |
| Technology skills   | plan   | Screen capture everything you create! |
| Learning skill 1    | -      | - |
| Learning skill 2    | -      | - |
| DevOps              | -      | - |
| Teamwork company 1  | -      | - |
| Teamwork company 2  | -      | - |
| Teamwork comparison | -      | Compare the two companies above. |


| Status | Description |
| ------ | ----------- |
| -      | Nothing planned yet |
| plan   | The *subject* contains an actionable plan. |
| wip    | The plan is currently being worked on. |
| review | This part is complete but needs checking by the graduation supervisor. |
| done   | The portfolio qualification part has been deemed complete by the graduation supervisor. |
