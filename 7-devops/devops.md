## DevOps

### Context description

*TODO* (A description of the context. What software did you set up the DevOps for? Was anything else used before that? What stack does the software use?)

### Automated (unit and/or end-to-end) testing

*TODO*:  (description, source code/screen shots/etc, motivation)

### Automated quality checks, such as linting and test coverage

*TODO*:  (description, source code/screen shots/etc, motivation)


### Containerization

*TODO*:  (description, source code/screen shots/etc, motivation)


### Continuous integration and/or deployment

*TODO*:  (description, source code/screen shots/etc, motivation)


-------------

**NOTE:** Feel free to change the above outline to better suit your work.

