# Technical design


## Introduction

*TODO* (About the company/product/context.)


## Project description

*TODO* (A summary of the functional design for this project. If part 1 of your portfolio is about this exact project, you can just refer to that.)


## High-level technical overview

*TODO* (For example using a C4 level 2 model.)


## Design choices

*TODO* (Including motivations and alternatives.)


## Security

*TODO*


## Scalability and reliability

*TODO*


## Help received

*TODO* (Describe any help you received while creating what's described in this portfolio part, including help from coworkers, classmates, friends, internet communities (passively reading forums doesn't need to be mentioned of course) or anybody else. Regular feedback from your graduation supervisor may be ignored, but if he/she contributed a very significant idea you should mention that as well. If you did it all by yourself, please state so explicitly. Failing to mention significant help received will be considered exam fraud.)

-------------

**NOTE:** Feel free to change the above outline to better suit your work.

