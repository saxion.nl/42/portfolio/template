# Functional design


## Introduction

*TODO* (About the company/product/context.)


## Problem statement

*TODO* (What did the customer/company/you hope to achieve that needed a functional design? What is the existing context (if any) that the design needed to fit in?)


## Domain model

*TODO*


## Wireframes

*TODO* (Images in the current directory or inline WireText. Don't forget to describe the behavior.)


## Validation

*TODO*


## Help received

*TODO* (Describe any help you received while creating what's described in this portfolio part, including help from coworkers, classmates, friends, internet communities (passively reading forums doesn't need to be mentioned of course) or anybody else. Regular feedback from your graduation supervisor may be ignored, but if he/she contributed a very significant idea you should mention that as well. If you did it all by yourself, please state so explicitly. Failing to mention significant help received will be considered exam fraud.)

-------------

**NOTE:** Feel free to change the above outline to better suit your work.

