## *TODO* (Challenge name)

### Introduction

*TODO* (Some general context about the project this challenge presented itself in.)

### Problem statement

*TODO* 

### Your process

*TODO* (What steps did you take to analyze and eventually solve the problem?)

### Resources

*TODO* (A list of resources you consulted.)

### Your solution

*TODO* (A concise description of your chosen solution.)

### Alternatives

*TODO* (A list of alternative solutions, if any, and their pros/cons.)

## Critical analysis

*TODO*

### Help received

*TODO* (Describe any help you received while creating what's described in this portfolio part, including help from coworkers, classmates, friends, internet communities (passively reading forums doesn't need to be mentioned of course) or anybody else. Regular feedback from your graduation supervisor may be ignored, but if he/she contributed a very significant idea you should mention that as well. If you did it all by yourself, please state so explicitly. Failing to mention significant help received will be considered exam fraud.)


-------------

**NOTE:** Feel free to change the above outline to better suit your work.

