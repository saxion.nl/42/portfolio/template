## *TODO* (Skill name)

### Context description

*TODO* (A description of the context. What skill were you lacking, and what did you need it for?)

### Basic introduction to the skill 

*TODO (An introduction to (the basics of) the skill in your own terms, in about 400 words. You may include short code examples, if that helps. The text should be targeted towards fellow students who completed the first year, but did not study this particular skill.)

### Your approach

*TODO* (How you approached learning the skill. Ideally you would be able to refer to specific parts of your journal here.)

-------------

**NOTE:** Feel free to change the above outline to better suit your work.

